Crowd.NET
=============

[![Official Site](https://img.shields.io/badge/site-Crowd.NET-blue.svg)](https://bitbucket.org/ianperrin/crowd.net) [![Latest version](https://img.shields.io/nuget/v/Crowd.NET.svg)](https://www.nuget.org/packages/Crowd.NET/) [![License MIT](https://img.shields.io/badge/license-MIT%20License-green.svg)](https://bitbucket.org/ianperrin/crowd.net/raw/master/LICENSE)

Crowd.NET is a Microsoft .NET library which provides a simple interface for interacting with the the [REST API resources](https://developer.atlassian.com/display/CROWDDEV/Crowd+REST+APIs) provided by [Atlassian Crowd](https://www.atlassian.com/software/crowd/overview).

The library allows a Microsoft .NET application (or ASP.NET/MVC website) to perform the following features via a Crowd server:

* Ability for applications to take advantage of SSO.
* User authentication.
* Retrieving, adding, updating and removing users.
* Retrieving, adding, updating and removing custom user attributes.
* Updating a user's password and requesting a password reset.
* Retrieving, adding, updating and removing groups.
* Retrieving, adding, updating and removing custom group attributes.
* Retrieving, adding, updating and removing group memberships.
* Retrieving, adding and removing nested group memberships.
* Access to the comprehensive search API. 

Installation
-------------

Crowd.NET is available as a [NuGet package](https://www.nuget.org/packages/Crowd.NET). So, you can install it using the NuGet Package Console window:

```
PM> Install-Package Crowd.NET
```

Usage
------
	
> **Atlassian Crowd configuration required**
> 
> Before using the Crowd.NET library you will need to create a *Generic Application* in the [Crowd Console](https://confluence.atlassian.com/display/CROWD/About+the+Crowd+Administration+Console). See [Adding an Application](https://confluence.atlassian.com/display/CROWD/Adding+an+Application) for more information.
> 
> Once the application has been created, you will need the base URL for your Crowd instance (e.g. http://localhost:8095/crowd/) along with the application name and password entered whilst creating the application to use the Crowd.NET library.


After installing the package, you can use the Crowd.NET library as follows:

```csharp

using Crowd.Rest.Client;
using Crowd.Rest.Client.Entity;

// ...

// Create a new instance of the Crowd REST Client Manager using the details for your application
CrowdRestClientManager cwd = new CrowdRestClientManager("CrowdBaseUrl", "YourApplicationName", "YourApplicationPassword");

// Call the required Crowd REST API resource method
User user = cwd.GetUser("SomeUserName");

// ...

```

Progress
-------
The Crowd REST API resources available in the Crowd.NET library will continue to be added until there is 100% coverage.

For those that are interest, here is a summary of the progress so far (based on the [Atlassian Crowd REST API](https://developer.atlassian.com/display/CROWDDEV/Crowd+REST+Resources) and the [Microsoft System.Web.Security](http://msdn.microsoft.com/en-us/library/System.Web.Security.aspx) docs):

![Overall Coverage - 93%](https://img.shields.io/badge/overall-93%25%20%2883%20of%2089%29-yellow.svg)

### Crowd REST resources 
![API Coverage - 98%](https://img.shields.io/badge/api-98%25%20%2859%20of%2060%29-yellow.svg)

* User resource: **100%** (17 of 17)
* Group resource: **95.8%** (23 of 24)
* User Authentication resource: **100%** (1 of 1)
* Search resource: **100%** (4 of 4)
* Session (SSO) resource: **100%** (8 of 8)
* Cookie Configuration resource: **100%** (1 of 1)
* Event resource: **100%** (2 of 2)
* Webhook resource: **100%** (3 of 3)

### ASP.NET Providers
![Provider Coverage - 83%](https://img.shields.io/badge/providers-83%25%20%2824%20of%2029%29-yellow.svg)

* Membership Provider: **72%** (13 of 18)
* Role Provider: **100%** (11 of 11)

Questions? Problems?
---------------------

Open-source project develop more smoothly when all discussions are held in public.

If you have any questions, problems related to the usage of Crowd.NET, ideas for new features, or think you've discovered a bug, please visit the [Issue Tracker](https://bitbucket.org/ianperrin/crowd.net/issues?status=new&status=open). You can sign in there using your existing BitBucket, Google, Facebook, twitter or GitHub account, so it's very simple to start using it.