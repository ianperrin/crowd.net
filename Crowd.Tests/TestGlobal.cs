﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Crowd.Tests
{
    /// <summary>
    /// Global test information is set in this class
    /// </summary>
    [TestClass]
    public class TestGlobal
    {
        /// <summary>
        /// The global base Uri for Crowd
        /// </summary>
        public static string Test_APIBaseUri = string.Empty;
        /// <summary>
        /// The global Application Name for testing
        /// </summary>
        public static string Test_ApplicationName = string.Empty;
        /// <summary>
        /// The global Application Password for testing
        /// </summary>
        public static string Test_ApplicationPassword = string.Empty;
        /// <summary>
        /// The global Bad Application Password for testing
        /// </summary>
        public static string Test_BadApplicationPassword = string.Empty;

        /// <summary>
        /// The global valid Username for testing
        /// </summary>
        public static string Test_UserName = string.Empty;
        /// <summary>
        /// The global valid password for Username for testing
        /// </summary>
        public static string Test_UserPassword = string.Empty;

        /// <summary>
        /// The global new User Name for testing
        /// </summary>
        public static string Test_NewUserName = string.Empty;

        /// <summary>
        /// The global valid Group Name for testing
        /// </summary>
        public static string Test_GroupName = string.Empty;

        /// <summary>
        /// The global new Group Name for testing
        /// </summary>
        public static string Test_NewGroupName = string.Empty;

        /// <summary>
        /// The global valid Email Address for testing
        /// </summary>
        public static string Test_EmailAddress = string.Empty;

        /// <summary>
        /// The global new Attribute Name for testing
        /// </summary>
        public static string Test_NewAttributeName = string.Empty;

        /// <summary>
        /// The global new Attribute Value for testing
        /// </summary>
        public static string Test_NewAttributeValue = string.Empty;
        
        [AssemblyInitialize()]
        public static void AllTestInit(TestContext testContext)
        {
            //  Set this to your base Uri for Crowd for testing
            Test_APIBaseUri = "http://localhost:4990/crowd";
            //  Set this to your Application Name for testing
            Test_ApplicationName = "Crowd.NET";
            //  Set this to your Application Password for testing
            Test_ApplicationPassword = "x4Ku4UST";
            //  Set this to a Valid UserName for testing
            Test_UserName = "admin";
            //  Set this to a valid passsword for UserName for testing
            Test_UserPassword = "admin";
            //  Set this to a New UserName for testing
            Test_NewUserName = "a.new.user";
            //  Set this to a Valid Email Address for testing
            Test_EmailAddress = "admin@example.com";
            //  Set this to a Valid Group Name for testing
            Test_GroupName = "crowd-administrators";
            //  Set this to a new Group Name for testing
            Test_NewGroupName = "crowd-net-test-users";
            //  Set this to a new Attribute Name for testing
            Test_NewAttributeName = "testAttribute";
            //  Set this to a new Attribute Value for testing
            Test_NewAttributeValue = "Test Attribute Value";
        }
    }
}
