﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Crowd.Rest.Client;
using Crowd.Rest.Client.Entity;

namespace Crowd.Tests
{
    [TestClass]
    public class EventTests
    {
        [TestMethod]
        public void GetCurrentEventToken_ShouldReturnString()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            string response = cwd.GetCurrentEventToken();

            //  Assert
            Assert.IsNotNull(response);

        }

        [TestMethod]
        public void GetNewEvents_WithActivity_ShouldReturnSameToken()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            string eventToken = cwd.GetCurrentEventToken();

            //  Act
            Events response = cwd.GetNewEvents(eventToken);

            //  Assert
            Assert.AreEqual(eventToken, response.NewEventToken);

        }
    }
}
