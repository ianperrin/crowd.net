﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Crowd.Rest.Client;
using Crowd.Rest.Client.Entity;

namespace Crowd.Tests
{
    [TestClass]
    public class CookieConfigTests
    {
        [TestMethod]
        public void GetCookieConfig_WithValidCredentials_ShouldReturnCookieConfig()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            CookieConfiguration response = cwd.GetCookieConfiguration();

            //  Assert
            Assert.IsNotNull(response.Name);
        }
        [TestMethod]
        [ExpectedException(typeof(CrowdApplicationPermissionException))]
        public void GetCookieConfig_WithInValidCredentials_ShouldThrowCrowdApplicationPermissionException()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_BadApplicationPassword);

            //  Act
            cwd.GetCookieConfiguration();

            // Assert is handled by ExpectedException
        }
    }
}
