﻿using Crowd.Web.Security;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace Crowd.Tests
{
    //http://highoncoding.com/Articles/379_Unit_Testing_Membership_and_Role_Providers.aspx
    //http://stackoverflow.com/questions/10314533/trying-to-test-custom-membership-provider
    public class MembershipTestBase
    {
        protected string _username;
        protected string _password;
        protected string _email;
        protected CrowdMembershipProvider _provider;
        protected NameValueCollection _config;
        protected MembershipCreateStatus _status = new MembershipCreateStatus();

        public void Initialize()
        {
            _username = TestGlobal.Test_NewUserName;
            _password = "P455w0rd!";
            _email = "test@example.com";

            // setup the membership provider
            _provider = new CrowdMembershipProvider();

            _config = new NameValueCollection();
            _config.Add("applicationName", "Crowd.NET Membership Test");
            _config.Add("name", "CrowdMembershipProvider");
            _config.Add("crowdApplicationUserName", TestGlobal.Test_ApplicationName);
            _config.Add("crowdApplicationPassword", TestGlobal.Test_ApplicationPassword);
            _config.Add("crowdBaseUrl", TestGlobal.Test_APIBaseUri);
            _provider.Initialize(_config["name"], _config);

            _status = new MembershipCreateStatus();

        }
    }
}
