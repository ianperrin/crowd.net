﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Crowd.Rest.Client;
using Crowd.Rest.Client.Entity;

namespace Crowd.Tests
{
    [TestClass]
    public class AuthenticationTests
    {
        [TestMethod]
        public void AuthenticateUser_WithValidCredentials_ShouldReturnTrue()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            User response = cwd.AuthenticateUser(TestGlobal.Test_UserName, TestGlobal.Test_UserPassword);

            //  Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(response.Name, TestGlobal.Test_UserName);
        }

        [TestMethod]
        [ExpectedException(typeof(CrowdInvalidAuthenticationException))]
        public void AuthenticateUser_WithInValidCredentials_ShouldReturnFalse()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            User response = cwd.AuthenticateUser(TestGlobal.Test_UserName, TestGlobal.Test_UserPassword.Insert(1, "INVALID"));

            //  Assert is handled by ExpectedException

        }

    }
}
