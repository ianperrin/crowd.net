﻿using Crowd.Web.Security;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace Crowd.Tests
{
    public class RoleTestBase
    {
        protected const string _testRole = "Test Role";
        protected CrowdRoleProvider _provider;
        protected NameValueCollection _config;

        public void Initialize()
        {
            // setup the role provider
            _provider = new CrowdRoleProvider();

            _config = new NameValueCollection();
            _config.Add("applicationName", "Crowd.NET Role Test");
            _config.Add("name", "CrowdRoleProvider");
            _config.Add("crowdApplicationUserName", TestGlobal.Test_ApplicationName);
            _config.Add("crowdApplicationPassword", TestGlobal.Test_ApplicationPassword);
            _config.Add("crowdBaseUrl", TestGlobal.Test_APIBaseUri);
            _config.Add("useNestedGroups", true.ToString());
            _provider.Initialize(_config["name"], _config);

        }
    }
}
