﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Security;
using Crowd.Web.Security;

namespace Crowd.Tests
{
    [TestClass]
    public class MembershipTests: MembershipTestBase
    {
        [TestMethod]
        public void MembershipProvider_CanCreateUser()
        {
            //  Arrange
            this.Initialize();

            // Act
            _provider.CreateUser(_username, _password, _email, null, null, true, null, out _status);

            // Assert
            Assert.AreEqual(MembershipCreateStatus.Success, _status);                
        }

        [TestMethod]
        public void MembershipProvider_CanValidateExistingUser()
        {
            //  Arrange
            this.Initialize();

            // Act
            bool response = _provider.ValidateUser(_username, _password);

            // Assert
            Assert.IsTrue(response);
        }


        [TestMethod]
        public void MembershipProvider_CannotValidateExistingUserWithBadPassword()
        {
            //  Arrange
            this.Initialize();

            // Act
            bool response = _provider.ValidateUser(_username, _password.Insert(1, "INVALID"));

            // Assert
            Assert.IsFalse(response);
        }
        
        [TestMethod]
        public void MembershipProvider_CanGetExistingUser()
        {
            //  Arrange
            this.Initialize();

            // Act
            MembershipUser response = _provider.GetUser(_username, false);
            CrowdMembershipUser castedResponse = response as CrowdMembershipUser;

            // Assert
            Assert.AreEqual(response.UserName, _username);
            Assert.IsTrue(castedResponse.LastLoginDate > DateTime.MinValue);
            Assert.AreEqual(1, castedResponse.InvalidPasswordAttempts);
        }

        [TestMethod]
        public void MembershipProvider_CanFindUsersByEmail()
        {
            //  Arrange
            this.Initialize();
            int totalRecords;

            // Act
            MembershipUserCollection response = _provider.FindUsersByEmail(_email, 0, Int32.MaxValue, out totalRecords);

            // Assert
            Assert.IsTrue(response.Count > 0);
            Assert.IsTrue(totalRecords > 0);
        }

        [TestMethod]
        public void MembershipProvider_CanDeleteExistingUser()
        {
            //  Arrange
            this.Initialize();

            // Act
            bool response = _provider.DeleteUser(_username, true);

            // Assert
            Assert.IsTrue(response);
        }


    }
}
