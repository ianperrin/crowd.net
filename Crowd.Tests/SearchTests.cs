﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Crowd.Rest.Client;
using Crowd.Rest.Client.Entity;
using System.Collections.Generic;

namespace Crowd.Tests
{
    [TestClass]
    public class SearchTests
    {
        [TestMethod]
        public void SearchUsers_WithoutRestriction_ShouldReturnAllUsers()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            List<User> response = cwd.SearchUsers();

            //  Assert
            Assert.IsTrue(response.Count > 0);
        }

        [TestMethod]
        public void SearchUsers_ByName_ShouldReturnUser()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            PropertySearchRestriction restriction = new PropertySearchRestriction
            {
                MatchMode = MatchMode.EXACTLY_MATCHES,
                Value = TestGlobal.Test_UserName,
                Property = new PropertyEntity { Name = "name", Type = PropertyType.STRING }
            };

            //  Act
            List<User> response = cwd.SearchUsers(restriction);

            //  Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Count >= 1);
            Assert.IsTrue(response[0].Name == TestGlobal.Test_UserName);
        }

        [TestMethod]
        public void SearchUsers_ByNameQuery_ShouldReturnUser()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            string restriction = string.Format(@"name=""{0}""", TestGlobal.Test_UserName);

            //  Act
            List<User> response = cwd.SearchUsers(restriction);

            //  Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Count >= 1);
            Assert.IsTrue(response[0].Name == TestGlobal.Test_UserName);
        }

        [TestMethod]
        public void SearchUsers_ByOrQuery_ShouldReturnUser()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            string restriction = string.Format(@"name=""{0}"" or firstName=""{0}"" or lastName=""{0}"" or displayName=""{0}"" or email=""{0}""", TestGlobal.Test_UserName);

            //  Act
            List<User> response = cwd.SearchUsers(restriction);

            //  Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Count >= 1);
        }

        [TestMethod]
        public void SearchUserNames_WithoutRestriction_ShouldReturnAllUsers()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            List<string> response = cwd.SearchUserNames();

            //  Assert
            Assert.IsTrue(response.Count > 0);
        }

        [TestMethod]
        public void SearchGroups_WithoutRestriction_ShouldReturnAllGroups()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            List<Group> response = cwd.SearchGroups();

            //  Assert
            Assert.IsTrue(response.Count >= 1);
        }
        [TestMethod]
        public void SearchGroups_ByName_ShouldReturnGroup()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            PropertySearchRestriction restriction = new PropertySearchRestriction
            {
                MatchMode = MatchMode.EXACTLY_MATCHES,
                Value = TestGlobal.Test_GroupName,
                Property = new PropertyEntity { Name = "name", Type = PropertyType.STRING }
            };

            //  Act
            List<Group> response = cwd.SearchGroups(restriction);

            //  Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Count >= 1);
            Assert.IsTrue(response[0].Name == TestGlobal.Test_GroupName);
        }
        [TestMethod]
        public void SearchGroups_ByAttribute_ShouldReturnGroup()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            // Store Attribute
            Crowd.Rest.Client.Entity.Attribute attribute = new Crowd.Rest.Client.Entity.Attribute { name = TestGlobal.Test_NewAttributeName, values = new List<string> { TestGlobal.Test_NewAttributeValue } };
            AttributeList attributes = new AttributeList { Attributes = new List<Crowd.Rest.Client.Entity.Attribute> { attribute } };
            cwd.StoreGroupAttributes(TestGlobal.Test_GroupName, attributes);

            // Build search
            PropertySearchRestriction restriction = new PropertySearchRestriction
            {
                MatchMode = MatchMode.EXACTLY_MATCHES,
                Value = TestGlobal.Test_NewAttributeValue,
                Property = new PropertyEntity { Name = TestGlobal.Test_NewAttributeName, Type = PropertyType.STRING }
            };

            //  Act
            List<Group> response = cwd.SearchGroups(restriction);

            //  Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Count >= 1);
            Assert.IsTrue(response[0].Name == TestGlobal.Test_GroupName);

            //  Cleanup
            cwd.RemoveGroupAttributes(TestGlobal.Test_GroupName, TestGlobal.Test_NewAttributeName);

        }        
        [TestMethod]
        public void SearchGroupNames_WithoutRestriction_ShouldReturnAllGroups()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            //  Act
            List<string> response = cwd.SearchGroupNames();

            //  Assert
            Assert.IsTrue(response.Count >= 1);
        }
        [TestMethod]
        public void SearchGroupsWithAttributes_ByName_ShouldReturnGroupWithAttibutes()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            Crowd.Rest.Client.Entity.Attribute attribute = new Crowd.Rest.Client.Entity.Attribute { name = TestGlobal.Test_NewAttributeName, values = new List<string> { TestGlobal.Test_NewAttributeValue } };
            AttributeList attributes = new AttributeList { Attributes = new List<Crowd.Rest.Client.Entity.Attribute> { attribute } };
            cwd.StoreGroupAttributes(TestGlobal.Test_GroupName, attributes);

            PropertySearchRestriction restriction = new PropertySearchRestriction
            {
                MatchMode = MatchMode.EXACTLY_MATCHES,
                Value = TestGlobal.Test_GroupName,
                Property = new PropertyEntity { Name = "name", Type = PropertyType.STRING }
            };

            //  Act
            List<GroupWithAttributes> response = cwd.SearchGroupsWithAttributes(restriction);

            //  Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Count >= 1);
            Assert.IsTrue(response[0].Name == TestGlobal.Test_GroupName);
            Assert.IsTrue(response[0].Attributes.Attributes.Count >= 1);

            // Clean up
            cwd.RemoveGroupAttributes(TestGlobal.Test_GroupName, TestGlobal.Test_NewAttributeName);

        }
    }
}
