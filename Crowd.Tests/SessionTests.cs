﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Crowd.Rest.Client;
using Crowd.Rest.Client.Entity;
using System.Collections.Generic;

namespace Crowd.Tests
{
    [TestClass]
    public class SessionTests
    {
        string _token;

        [TestMethod]
        public void AuthenticateSSOUser_ShouldReturnToken()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            string LocalIP = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();
            List<ValidationFactor> v = new List<ValidationFactor>();
            v.Add(new ValidationFactor { name = "remote_address", value = LocalIP });
            ValidationFactors validationFactors = new ValidationFactors { validationFactors = v};
            AuthenticationContext authContext = new AuthenticationContext { UserName = TestGlobal.Test_UserName,
                                                                            Password = TestGlobal.Test_UserPassword,
                                                                            ValidationFactors = validationFactors
            };
            //  Act
            string response = cwd.AuthenticateSSOUser(authContext);

            //  Assert
            Assert.IsNotNull(response);
            _token = response;
        }

        [TestMethod]
        public void AuthenticateSSOUser_WithNullValidationFactors_ShouldReturnToken()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            AuthenticationContext authContext = new AuthenticationContext
            {
                UserName = TestGlobal.Test_UserName,
                Password = TestGlobal.Test_UserPassword,
                ValidationFactors = null
            };

            //  Act
            string response = cwd.AuthenticateSSOUser(authContext);

            //  Assert
            Assert.IsNotNull(response);
            _token = response;
        }

        [TestMethod]
        public void AuthenticateSSOUser_WithNoValidationFactors_ShouldReturnToken()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            AuthenticationContext authContext = new AuthenticationContext
            {
                UserName = TestGlobal.Test_UserName,
                Password = TestGlobal.Test_UserPassword
            };

            //  Act
            string response = cwd.AuthenticateSSOUser(authContext);

            //  Assert
            Assert.IsNotNull(response);
            _token = response;
        }

        [TestMethod]
        public void AuthenticateSSOUser_WithNewValidationFactors_ShouldReturnToken()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            AuthenticationContext authContext = new AuthenticationContext
            {
                UserName = TestGlobal.Test_UserName,
                Password = TestGlobal.Test_UserPassword,
                ValidationFactors = new ValidationFactors()
            };

            //  Act
            string response = cwd.AuthenticateSSOUser(authContext);

            //  Assert
            Assert.IsNotNull(response);
            _token = response;
        }

        [TestMethod]
        public void AuthenticateSSOUser_WithEmptyValidationFactors_ShouldReturnToken()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            AuthenticationContext authContext = new AuthenticationContext
            {
                UserName = TestGlobal.Test_UserName,
                Password = TestGlobal.Test_UserPassword,
                ValidationFactors = new ValidationFactors { validationFactors = new List<ValidationFactor>() }
            };

            //  Act
            string response = cwd.AuthenticateSSOUser(authContext);

            //  Assert
            Assert.IsNotNull(response);
            _token = response;
        }

        [TestMethod]
        public void FindUserFromSSOToken_ShouldReturnUser()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            AuthenticateSSOUser_ShouldReturnToken();
            string token = _token;

            //  Act
            User response = cwd.FindUserFromSSOToken(token);

            //  Assert
            Assert.IsNotNull(response, "Response is null, but expecting :" + token);
            Assert.AreEqual(TestGlobal.Test_UserName, response.Name);
        }

        [TestMethod]
        public void ValidateSSOAuthentication_WithNoValidationFactors_ShouldNotFail()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            AuthenticationContext authContext = new AuthenticationContext
            {
                UserName = TestGlobal.Test_UserName,
                Password = TestGlobal.Test_UserPassword
            };
            string token = cwd.AuthenticateSSOUser(authContext);

            try
            {
                //  Act
                cwd.ValidateSSOAuthentication(token);
            }
            catch
            {
                //  Assert
                Assert.Fail();
            }

        }

        [TestMethod]
        public void ValidateSSOAuthentication_WithNoValidationFactors_ShouldReturnSession()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);
            AuthenticationContext authContext = new AuthenticationContext
            {
                UserName = TestGlobal.Test_UserName,
                Password = TestGlobal.Test_UserPassword
            };
            string token = cwd.AuthenticateSSOUser(authContext);

            //  Act
            SSOSession response = cwd.ValidateSSOAuthenticationAndGetSession(token);
            
            //  Assert
            Assert.IsNotNull(response, "Response is null, but expecting an SSO session with token:" + token);
            Assert.AreEqual(token, response.Token);

        }

        [TestMethod]
        public void InvalidateTokens_WithUser_ShouldNotFail()
        {
            //  Arrange
            CrowdRestClientManager cwd = new CrowdRestClientManager(TestGlobal.Test_APIBaseUri, TestGlobal.Test_ApplicationName, TestGlobal.Test_ApplicationPassword);

            try
            {
                //  Act
                cwd.InvalidateSSOTokensForUser(TestGlobal.Test_UserName);
            }
            catch
            {
                //  Assert
                Assert.Fail();
            }

        }


    }
}
