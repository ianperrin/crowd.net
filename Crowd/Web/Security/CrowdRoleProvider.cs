﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using Crowd.Rest.Client;
using Crowd.Rest.Client.Entity;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Net;

namespace Crowd.Web.Security
{
    /// <summary>
    /// An ASP.NET RoleProvider for a remote Atlassian Crowd Server.
    /// </summary>
    public class CrowdRoleProvider: RoleProvider
    {
        #region RoleProvider Fields and Properties
        // keeps track of whether the provider has already been initialized 
        private bool initialized = false;
        // configuration parameters common to all role providers 
        private string appName;
        private bool useNestedGroups;

        /// <summary>
        /// Gets or sets the name of the application to store and retrieve role information for.
        /// </summary>
        /// <returns>The name of the application to store and retrieve role information for.</returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        /// <exception cref="System.NotSupportedException"></exception>
        public override string ApplicationName
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

                return appName;
            }
            set
            {
                throw new NotSupportedException(string.Format(exceptionMessageSettingNotSupported, "ApplicationName"));
            }
        }
        /// <summary>
        /// Gets a value indicating whether enable nested group support for the application.
        /// </summary>
        /// <value>
        /// <c>true</c> if nested groups should be enabled; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref="System.InvalidOperationException"></exception>
        public bool UseNestedGroups
        {
            get
            {
                if (!initialized)
                    throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

                return useNestedGroups;
            }
        }

        // crowd client
        private CrowdRestClientManager crowd;
        // provider exception messages 
        private const string exceptionMessageProviderNotInitialized = "The provider has not been initialized.";
        private const string exceptionMessageSettingNotSupported = "The setting <{0}> is not supported by this provider.";
        private const string exceptionMessageSettingMustNotBeEmpty = "The setting <{0}> must not be empty.";
        private const string exceptionMessageSettingIsRequired = "The setting <{0}> is required.";
        private const string exceptionMessageSettingUnrecognized = "The setting <{0}> is not recognized.";
        private const string exceptionMessageRoleIsNotEmpty = "The role <{0}> is not empty.";

        #endregion

        #region ProviderBase Methods

        /// <summary>
        /// Initializes the provider.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        /// <exception cref="System.ArgumentNullException">config</exception>
        /// <exception cref="System.Configuration.Provider.ProviderException"></exception>
        public override void Initialize(string name, NameValueCollection config)
        {
            if (initialized)
                return;

            // Initialize values from web.config.
            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "CrowdRoleProvider";

            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Role Provider for Atlassian Crowd");
            }
            base.Initialize(name, config);

            try
            {
                this.appName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);

                // credentials 
                // baseurl, username and password if specified must not be empty, moreover if one is specified the others must
                // be specified as well
                string baseUrl = config["crowdBaseUrl"];
                if (baseUrl != null && baseUrl.Length == 0)
                    throw new ProviderException(string.Format(exceptionMessageSettingMustNotBeEmpty, "crowdBaseUrl"));

                string username = config["crowdApplicationUserName"];
                if (username != null && username.Length == 0)
                    throw new ProviderException(string.Format(exceptionMessageSettingMustNotBeEmpty, "crowdApplicationUserName"));

                string password = config["crowdApplicationPassword"];
                if (password != null && password.Length == 0)
                    throw new ProviderException(string.Format(exceptionMessageSettingMustNotBeEmpty, "crowdApplicationPassword"));

                if ((username != null && password == null) || (password != null && username == null))
                    throw new ProviderException(string.Format(exceptionMessageSettingIsRequired, "Crowd Application Credentials"));
                NetworkCredential credential = new NetworkCredential(username, password, baseUrl);

                useNestedGroups = Convert.ToBoolean(GetConfigValue(config["useNestedGroups"], "true"));

                // Initialise Crowd Client
                crowd = new CrowdRestClientManager(credential.Domain, credential.UserName, credential.Password);

                // Check configuration
                config.Remove("name");
                config.Remove("applicationName");
                config.Remove("description");

                config.Remove("crowdBaseUrl");
                config.Remove("crowdApplicationUserName");
                config.Remove("crowdApplicationPassword");
                config.Remove("useNestedGroups");

                if (config.Count > 0)
                {
                    string attribUnrecognized = config.GetKey(0);
                    if (!string.IsNullOrEmpty(attribUnrecognized))
                        throw new ProviderException(string.Format(exceptionMessageSettingUnrecognized, attribUnrecognized));
                }

                initialized = true;
            }
            catch (Exception ex)
            {
                throw new ProviderException(exceptionMessageProviderNotInitialized, ex);
            }
        
        }
        #endregion

        #region RoleProvider Methods
        /// <summary>
        /// Adds the specified user names to the specified roles for the configured applicationName.
        /// </summary>
        /// <param name="usernames">A string array of user names to be added to the specified roles.</param>
        /// <param name="roleNames">A string array of the role names to add the specified user names to.</param>
        /// <exception cref="System.InvalidOperationException"></exception>
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            foreach (string username in usernames)
            {
                foreach (string roleName in roleNames)
                {
                    // TODO: AddUsersToRoles - Should roll-back method if any AddUserToGroup call fails
                    try
                    {
                        crowd.AddUserToGroup(username, roleName);
                    }
                    catch (CrowdGroupNotFoundException)
                    {
                        // The group could not be found !
                        throw;
                    }
                    catch (CrowdUserNotFoundException)
                    {
                        // the user could not be found
                        throw;
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Adds a new role to the data source for the configured applicationName.
        /// </summary>
        /// <param name="roleName">The name of the role to create.</param>
        /// <exception cref="System.InvalidOperationException"></exception>
        public override void CreateRole(string roleName)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            try
            {
                Group group = new Group { Name = roleName, active = true, type = "GROUP" };
                crowd.AddGroup(group);
            }
            catch (CrowdInvalidGroupException)
            {
                // The group already exists
                throw;
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Removes a role from the data source for the configured applicationName.
        /// </summary>
        /// <param name="roleName">The name of the role to delete.</param>
        /// <param name="throwOnPopulatedRole">If true, throw an exception if <paramref name="roleName" /> has one or more members and do not delete <paramref name="roleName" />.</param>
        /// <returns>
        /// true if the role was successfully deleted; otherwise, false.
        /// </returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        /// <exception cref="System.Configuration.Provider.ProviderException"></exception>
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            try
            {
                if (throwOnPopulatedRole)
                {
                    string[] members = GetUsersInRole(roleName);
                    if (members.Count() > 0)
                        throw new ProviderException(string.Format(exceptionMessageRoleIsNotEmpty, roleName));

                }
                    
                crowd.RemoveGroup(roleName);
            }
            catch
            {
                throw;
            }

            return true;
        }

        /// <summary>
        /// Gets an array of user names in a role where the user name contains the specified user name to match.
        /// </summary>
        /// <param name="roleName">The role to search in.</param>
        /// <param name="usernameToMatch">The user name to search for.</param>
        /// <returns>
        /// A string array containing the names of all the users where the user name matches <paramref name="usernameToMatch" /> and the user is a member of the specified role.
        /// </returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        /// <exception cref="System.Configuration.Provider.ProviderException"></exception>
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            try
            {
                List<User> users = crowd.GetUsersOfGroup(roleName);
                if (this.useNestedGroups) users.AddRange(crowd.GetNestedUsersOfGroup(roleName));
                
                var usersWhichMatch = from u in users
                                     where u.Name.Contains(usernameToMatch)
                                               || u.DisplayName.Contains(usernameToMatch)
                                               || u.Email.Contains(usernameToMatch)
                                               || u.FirstName.Contains(usernameToMatch)
                                               || u.LastName.Contains(usernameToMatch)
                                        select u;

                return usersWhichMatch.Select(i => i.Name).ToArray();
            }
            catch (CrowdGroupNotFoundException ex)
            {
                // The group could not be found
                throw new ProviderException(ex.Message, ex);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Gets a list of all the roles for the configured applicationName.
        /// </summary>
        /// <returns>
        /// A string array containing the names of all the roles stored in the data source for the configured applicationName.
        /// </returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        public override string[] GetAllRoles()
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            try
            {
                List<string> groups = crowd.SearchGroupNames(null, 0, -1);

                return groups.ToArray();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Gets a list of the roles that a specified user is in for the configured applicationName.
        /// </summary>
        /// <param name="username">The user to return a list of roles for.</param>
        /// <returns>
        /// A string array containing the names of all the roles that the specified user is in for the configured applicationName.
        /// </returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        /// <exception cref="System.Configuration.Provider.ProviderException"></exception>
        public override string[] GetRolesForUser(string username)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            try
            {
                List<string> groups = crowd.GetNamesOfGroupsForUser(username);
                if (this.useNestedGroups) groups.AddRange(crowd.GetNamesOfGroupsForNestedUser(username));

                return groups.ToArray();
            }
            catch (CrowdUserNotFoundException ex)
            {
                // The user could not be found
                throw new ProviderException(ex.Message, ex);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Gets a list of users in the specified role for the configured applicationName.
        /// </summary>
        /// <param name="roleName">The name of the role to get the list of users for.</param>
        /// <returns>
        /// A string array containing the names of all the users who are members of the specified role for the configured applicationName.
        /// </returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        /// <exception cref="System.Configuration.Provider.ProviderException"></exception>
        public override string[] GetUsersInRole(string roleName)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            try
            {
                List<string> users = crowd.GetNamesOfUsersOfGroup(roleName);
                if (this.useNestedGroups) users.AddRange(crowd.GetNamesOfNestedUsersOfGroup(roleName));

                return users.ToArray();
            }
            catch (CrowdGroupNotFoundException ex)
            {
                // The group could not be found
                throw new ProviderException(ex.Message, ex);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the specified user is in the specified role for the configured applicationName.
        /// </summary>
        /// <param name="username">The user name to search for.</param>
        /// <param name="roleName">The role to search in.</param>
        /// <returns>
        /// true if the specified user is in the specified role for the configured applicationName; otherwise, false.
        /// </returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        public override bool IsUserInRole(string username, string roleName)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            bool result = false;

            try
            {
                // Check Direct Memberships
                result = crowd.IsUserDirectGroupMember(username, roleName);
                if (result)
                    return result;

                // Check Nested Memberships
                if (this.useNestedGroups)
                {
                    result = crowd.IsUserNestedGroupMember(username, roleName);
                    if (result)
                        return result;
                }
            }
            catch
            {
                throw;
            }

            return false;

        }

        /// <summary>
        /// Removes the specified user names from the specified roles for the configured applicationName.
        /// </summary>
        /// <param name="usernames">A string array of user names to be removed from the specified roles.</param>
        /// <param name="roleNames">A string array of role names to remove the specified user names from.</param>
        /// <exception cref="System.InvalidOperationException"></exception>
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            foreach (string username in usernames)
            {
                foreach (string roleName in roleNames)
                {
                    // TODO: RemoveUsersFromRoles - Should roll-back method if any RemoveUserFromGroup call fails
                    try
                    {
                        crowd.RemoveUserFromGroup(username, roleName);
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the specified role name already exists in the role data source for the configured applicationName.
        /// </summary>
        /// <param name="roleName">The name of the role to search for in the data source.</param>
        /// <returns>
        /// true if the role name already exists in the data source for the configured applicationName; otherwise, false.
        /// </returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        public override bool RoleExists(string roleName)
        {
            if (!initialized)
                throw new InvalidOperationException(exceptionMessageProviderNotInitialized);

            // Determine whether the role exists
            bool roleExists = false;
            try
            {
                Group group = crowd.GetGroup(roleName);
                roleExists = (group != null && group.Name == roleName);
            }
            catch (CrowdGroupNotFoundException)
            {
                // The group does not exist
                roleExists = false;
            }
            catch (CrowdRestException)
            {
                throw;
            }
            catch
            {
                throw;
            }

            return roleExists;
        }
        #endregion

        #region Helper Methods and Properties
        /// <summary>
        /// Gets the configuration value.
        /// </summary>
        /// <param name="configValue">The configuration value.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        private string GetConfigValue(string configValue, string defaultValue = "")
        {
            //_logger.Debug(string.Format("Getting {0} config value: '{1}'; defaulting to: '{2}'", this.Name, configValue, defaultValue));

            if (string.IsNullOrWhiteSpace(configValue))
                return defaultValue;

            return configValue;
        }
        #endregion
    }
}
