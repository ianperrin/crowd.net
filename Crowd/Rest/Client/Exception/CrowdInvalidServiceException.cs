using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when a Crowd client is not communicating with a valid Crowd service.
    /// @since v2.1
    /// </summary>
    public class CrowdInvalidServiceException : CrowdOperationFailedException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdInvalidServiceException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public CrowdInvalidServiceException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdInvalidServiceException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdInvalidServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}