using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// 
    /// </summary>
    public class CrowdFailedAuthenticationException : CrowdException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdFailedAuthenticationException"/> class.
        /// </summary>
        public CrowdFailedAuthenticationException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdFailedAuthenticationException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public CrowdFailedAuthenticationException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdFailedAuthenticationException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdFailedAuthenticationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdFailedAuthenticationException"/> class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        public CrowdFailedAuthenticationException(Exception innerException)
            : base(innerException)
        {
        }
    }
}