using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// <code>CrowdException</code> is the base class of Crowd-specific exceptions that must be caught.
    /// This allows consumers of Crowd services to catch all checked exceptions with only one catch block.
    /// </summary>
    public abstract class CrowdException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdException"/> class.
        /// </summary>
        public CrowdException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public CrowdException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public CrowdException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdException"/> class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        public CrowdException(Exception innerException)
            : base(innerException.Message, innerException)
        {
            // TODO: Check default message for CrowdException(Exception innerException) constructor
        }
    }
}