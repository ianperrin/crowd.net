using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when an event token is either not recognised or has expired.
    /// </summary>
    public class CrowdEventTokenExpiredException : CrowdException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdEventTokenExpiredException"/> class.
        /// </summary>
        public CrowdEventTokenExpiredException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdEventTokenExpiredException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public CrowdEventTokenExpiredException(string message)
            : base(message)
        {
        }
    }
}