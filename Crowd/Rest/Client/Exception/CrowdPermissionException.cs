using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Permission Exception this Exception will handle Exceptions to do with CRUD operations
    /// on Applications, Directories etc.
    /// </summary>
    public abstract class CrowdPermissionException : CrowdException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdPermissionException"/> class.
        /// </summary>
        public CrowdPermissionException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdPermissionException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public CrowdPermissionException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdPermissionException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public CrowdPermissionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdPermissionException"/> class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        public CrowdPermissionException(Exception innerException)
            : base(innerException)
        {
        }
    }
}