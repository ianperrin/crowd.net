using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when an entity is not found.
    /// </summary>
    public class CrowdObjectNotFoundException : CrowdException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdObjectNotFoundException"/> class.
        /// </summary>
        public CrowdObjectNotFoundException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdObjectNotFoundException"/> class.
        /// </summary>
        /// <param name="entityClass">The entity class.</param>
        /// <param name="identifier">The identifier.</param>
        public CrowdObjectNotFoundException(object entityClass, object identifier)
            : base(string.Format("Failed to find entity of type [{0}] with identifier [{1}]", entityClass.GetType(), identifier.ToString()))
        {
        }

        /// <summary>
        /// Constructs a new exception with the specified detail message.
        /// </summary>
        /// <param name="message">the detail message</param>
        public CrowdObjectNotFoundException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructs a new exception with the specified detail message and cause.
        /// </summary>
        /// <param name="message">the detail message</param>
        /// <param name="innerException">the inner exception</param>
        public CrowdObjectNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdObjectNotFoundException"/> class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        public CrowdObjectNotFoundException(Exception innerException)
            : base(innerException)
        {
        }
    }
}