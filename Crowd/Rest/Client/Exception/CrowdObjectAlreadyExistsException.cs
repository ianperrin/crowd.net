using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// @since v2.7
    /// </summary>
    public class CrowdObjectAlreadyExistsException : CrowdException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdObjectAlreadyExistsException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public CrowdObjectAlreadyExistsException(string message)
            : base(message)
        {
        }
    }
}