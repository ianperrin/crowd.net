using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when the supplied credential is not valid.
    /// </summary>
    public class CrowdInvalidCredentialException : CrowdException
    {
        /// <summary>
        /// a description of the policy that has been violated, if available. If such description is
        /// not available, this method returns null. In that case, refer to <see cref="PolicyDescription"/> for a general
        /// description of the exception.
        /// </summary>
        public string PolicyDescription { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdInvalidCredentialException"/> class.
        /// </summary>
        public CrowdInvalidCredentialException()
        {
            this.PolicyDescription = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdInvalidCredentialException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public CrowdInvalidCredentialException(string message)
            : base(message)
        {
            this.PolicyDescription = null;
        }

        /// <summary>
        /// Use this constructor when you can identify a specific policy that has been violated.
        /// If the policy is not known, use one of the other constructors.
        /// </summary>
        /// <param name="genericMessage">a general message describing how this exception happened</param>
        /// <param name="policyDescription">a message describing the policy that has been violated</param>
        public CrowdInvalidCredentialException(string genericMessage, string policyDescription)
            : base(policyDescription == null ? genericMessage : genericMessage + ": " + policyDescription)
        {
            this.PolicyDescription = policyDescription;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdInvalidCredentialException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public CrowdInvalidCredentialException(string message, Exception innerException)
            : base(message, innerException)
        {
            this.PolicyDescription = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdInvalidCredentialException"/> class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        public CrowdInvalidCredentialException(Exception innerException)
            : base(innerException)
        {
            this.PolicyDescription = null;
        }

    }
}