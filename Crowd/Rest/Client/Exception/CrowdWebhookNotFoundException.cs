using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when the specified Webhook could not be found.
    /// </summary>
    public class CrowdWebhookNotFoundException : CrowdObjectNotFoundException
    {
        /// <summary>
        /// Gets the webhook identifier.
        /// </summary>
        /// <value>
        /// The webhook identifier.
        /// </value>
        public long WebhookId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdWebhookNotFoundException"/> class.
        /// </summary>
        /// <param name="webhookId">The webhook identifier.</param>
        public CrowdWebhookNotFoundException(long webhookId)
            : this(webhookId, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdWebhookNotFoundException"/> class.
        /// </summary>
        /// <param name="webhookId">The webhook identifier.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdWebhookNotFoundException(long webhookId, Exception innerException)
            : base("Webhook <" + webhookId.ToString() + "> does not exist", innerException)
        {
            this.WebhookId = webhookId;
        }
    }
}