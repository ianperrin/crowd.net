using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown when an invalid token is provided.
    /// </summary>
    public class CrowdInvalidTokenException : CrowdException
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CrowdInvalidTokenException()
            : base()
        {
        }

        public CrowdInvalidTokenException(string message)
            : base(message)
        {
        }

        public CrowdInvalidTokenException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public CrowdInvalidTokenException(Exception innerException)
            : base(innerException)
        {
        }
    }
}