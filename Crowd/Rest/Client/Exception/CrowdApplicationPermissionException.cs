using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown to indicate that an Application does not have the required permission to perform the operation.
    /// </summary>
    public class CrowdApplicationPermissionException : CrowdPermissionException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdApplicationPermissionException"/> class.
        /// </summary>
        public CrowdApplicationPermissionException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdApplicationPermissionException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public CrowdApplicationPermissionException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdApplicationPermissionException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdApplicationPermissionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdApplicationPermissionException"/> class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        public CrowdApplicationPermissionException(Exception innerException)
            : base(innerException)
        {
        }
    }
}
