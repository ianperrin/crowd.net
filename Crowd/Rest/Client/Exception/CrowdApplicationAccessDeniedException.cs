using System;

namespace Crowd.Rest.Client
{

    /// <summary>
    /// Thrown to indicate that a user does not have access to authenticate against an application.
    /// </summary>
    public class CrowdApplicationAccessDeniedException : CrowdException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdApplicationAccessDeniedException"/> class.
        /// </summary>
        public CrowdApplicationAccessDeniedException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdApplicationAccessDeniedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public CrowdApplicationAccessDeniedException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdApplicationAccessDeniedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public CrowdApplicationAccessDeniedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdApplicationAccessDeniedException"/> class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        public CrowdApplicationAccessDeniedException(Exception innerException)
            : base(innerException)
        {
        }
    }
}