﻿using System.Runtime.Serialization;

namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Class for error response from the Crowd API
    /// </summary>
    [DataContract]
    public class Error
    {
        /// <summary>
        /// Error responses received from the Crowd REST API.
        /// See https://developer.atlassian.com/display/CROWDDEV/Using+the+Crowd+REST+APIs#UsingtheCrowdRESTAPIs-HTTPResponseCodesandErrorResponses
        /// See https://docs.atlassian.com/crowd/2.8.0/com/atlassian/crowd/plugin/rest/entity/ErrorEntity.ErrorReason.html
        /// </summary>
        public enum ErrorReason
        {
            /// <summary>
            /// User does not have access to a particular application
            /// </summary>
            APPLICATION_ACCESS_DENIED,
            APPLICATION_MODIFICATION_FAILED,
            APPLICATION_NOT_FOUND,
            /// <summary>
            /// Application does not have permission to perform the operation
            /// </summary>
            APPLICATION_PERMISSION_DENIED,
            DIRECTORY_NOT_FOUND,
            EVENT_TOKEN_EXPIRED,
            /// <summary>
            /// User credentials have expired
            /// </summary>
            EXPIRED_CREDENTIAL,
            /// <summary>
            /// Group is not found
            /// </summary>
            GROUP_NOT_FOUND,
            /// <summary>
            /// REST method is given an illegal argument
            /// </summary>
            ILLEGAL_ARGUMENT,
            /// <summary>
            /// User account is inactive
            /// </summary>
            INACTIVE_ACCOUNT,
            INCREMENTAL_SYNC_NOT_AVAILABLE,
            /// <summary>
            /// The supplied credential is not valid. E.g. no password given when adding a user
            /// </summary>
            INVALID_CREDENTIAL,
            /// <summary>
            /// Given email address is not valid
            /// </summary>
            INVALID_EMAIL,
            /// <summary>
            /// Given group is invalid. E.g. unknown group type, adding a group that already exists
            /// </summary>
            INVALID_GROUP,
            INVALID_MEMBERSHIP,
            /// <summary>
            /// Given SSO token is invalid
            /// </summary>
            INVALID_SSO_TOKEN,
            /// <summary>
            /// Given user is invalid. E.g. adding a user that already exists
            /// </summary>
            INVALID_USER,
            /// <summary>
            /// Username/password combination for authentication is invalid
            /// </summary>
            INVALID_USER_AUTHENTICATION,
            MEMBERSHIP_ALREADY_EXISTS,
            /// <summary>
            /// When the child-parent relationship does not exist
            /// </summary>
            MEMBERSHIP_NOT_FOUND,
            /// <summary>
            /// Nested groups are not supported
            /// </summary>
            NESTED_GROUPS_NOT_SUPPORTED,
            /// <summary>
            /// Operation failed for any other reason
            /// </summary>
            OPERATION_FAILED,
            PERMISSION_DENIED,
            /// <summary>
            /// Requested operation is not supported
            /// </summary>
            UNSUPPORTED_OPERATION,
            /// <summary>
            /// User not found
            /// </summary>
            USER_NOT_FOUND,
            WEBHOOK_NOT_FOUND
        }

        [DataMember(Name = "reason")]
        public ErrorReason Reason { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }

    }
}
