﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes a link response.
    /// </summary>
    public class Link
    {
        public string href { get; set; }
        public string rel { get; set; }
    }
}
