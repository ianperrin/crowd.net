﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crowd.Rest.Client.Entity
{
    public class ValidationFactor
    {
        public string name { get; set; }
        public string value { get; set; }
    }

}
