﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Crowd.Rest.Client.Entity
{
    [DataContract]
    public class SSOSession
    {
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        [DataMember(Name = "expand")]
        public string Expand { get; set; }
        [DataMember(Name="link")]
        public Link Link { get; set; }
        [DataMember(Name = "token")]
        public string Token { get; set; }
        [DataMember(Name = "user")]
        public User User { get; set; }
        [DataMember(Name = "created-date")]
        public long CreatedDateUnixTimestamp { get; set; }
        [DataMember(Name = "expiry-date")]
        public long ExpiryDateUnixTimestamp { get; set; }

        public DateTime CreatedDate => (Epoch + TimeSpan.FromMilliseconds(CreatedDateUnixTimestamp)).ToLocalTime();
        public DateTime ExpiryDate => (Epoch + TimeSpan.FromMilliseconds(ExpiryDateUnixTimestamp)).ToLocalTime();
    }
}
