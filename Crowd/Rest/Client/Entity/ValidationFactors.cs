﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class ValidationFactors
    {
        /// <summary>
        /// Gets or sets the validation factors.
        /// </summary>
        /// <value>
        /// The validation factors.
        /// </value>
        public List<ValidationFactor> validationFactors { get; set; }
    }
}
