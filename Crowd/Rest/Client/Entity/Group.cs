﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes a Group.
    /// </summary>
    [DataContract(Name = "group", Namespace = "")]
    public class Group
    {
        [DataMember(Name = "expand")]
        public string Expand { get; set; }
        [DataMember(Name = "link")]
        public Link Link { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "description")]
        public string Description { get; set; }
        [DataMember(Name = "type")]
        public string type { get; set; }
        [DataMember(Name = "active")]
        public bool active { get; set; }
    }

}
