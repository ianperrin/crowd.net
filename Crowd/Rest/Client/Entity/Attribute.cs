﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes an Attribute.
    /// </summary>
    public class Attribute
    {
        public Link link { get; set; }
        public string name { get; set; }
        public List<string> values { get; set; }
    }
}
