﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Crowd.Rest.Client.Entity
{
    /// <summary>
    /// Describes a Group List.
    /// </summary>
    [DataContract(Name = "groups")]
    public class GroupList
    {

        [DataMember(Name = "groups")]
        public List<Group> Groups { get; set; }
        [DataMember(Name = "expand")]
        public string Expand { get; set; }

    }
}
