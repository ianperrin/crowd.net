﻿using System;
using System.Collections.Generic;
using System.Net;
using ServiceStack.Text;
using Crowd.Rest.Client.Entity;

namespace Crowd.Rest.Client
{
    public partial class CrowdRestClientManager
    {

        #region API : Authentication

        /// <summary>
        /// Authenticates a user with the server.
        /// </summary>
        /// <param name="username">The username of the user to authenticate.</param>
        /// <param name="password">The password of the user to authenticate.</param>
        /// <returns>
        /// True if the user is correctly authenticated
        /// </returns>
        /// <exception cref="CrowdUserNotFoundException">if the user could not be found.</exception>
        /// <exception cref="CrowdInactiveAccountException">if the user account is not active.</exception>
        /// <exception cref="CrowdExpiredCredentialException">if the user credentials have expired.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public User AuthenticateUser(string username, string password)
        {
            try
            {
                // Our api resource
                string apiResource = "authentication.json?username={0}".Fmt(username.UrlEncode());
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = new Password { value = password };

                //  Make the call:
                return MakeAPICall<User>(apiResource, httpMethod, args);

            }
            catch (CrowdRestException e)
            {
                HandleUserNotFound(e.Error, username);
                HandleInactiveUserAccount(e.Error, username);
                HandleExpiredUserCredential(e.Error);
                HandleInvalidUserAuthentication(e.Error, username);
                throw HandleCommonExceptions(e);
            }
        }

        #endregion

    }
}

