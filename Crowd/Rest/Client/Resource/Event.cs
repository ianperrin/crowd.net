﻿using System;
using System.Collections.Generic;
using System.Net;
using ServiceStack.Text;
using Crowd.Rest.Client.Entity;

namespace Crowd.Rest.Client
{
    public partial class CrowdRestClientManager
    {

        #region API : Event

        /// <summary>
        /// Gets a token that can be used for querying events that have happened after the token was generated.
        /// </summary>
        /// <returns>A token that can be used for querying events that have happened after the token was generated</returns>
        /// <remarks>
        /// If the event token has not changed since the last call to this method, it is guaranteed 
        /// that no new events have been received.
        /// The format of event token is implementation specific and can change without a warning.
        /// </remarks>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdUnsupportedApiException">if the remote server does not support this operation.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments.</exception>
        /// <exception cref="CrowdIncrementalSynchronisationNotAvailableException">if the application cannot provide incremental synchronisation</exception>
        public string GetCurrentEventToken()
        {
            try
            {
                // Our api resource
                string apiResource = "event.json";
                string httpMethod = "GET";

                //  Make the call:
                Events events = MakeAPICall<Events>(apiResource, httpMethod);

                if (events.IsIncrementalSynchronisationAvailable == null)
                {
                    throw new CrowdIncrementalSynchronisationNotAvailableException("Incremental synchronisation is not guaranteed to be available");
                }
                else if (!(bool)events.IsIncrementalSynchronisationAvailable)
                {
                    throw new CrowdIncrementalSynchronisationNotAvailableException();
                }

                return events.NewEventToken;
            }
            catch (CrowdRestException e)
            {
                // The REST call failed, but we're talking to Crowd because otherwise ICSE would have been thrown
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    throw new CrowdUnsupportedApiException("1.2", "for event-based synchronisation");
                }
                else
                {
                    throw HandleCommonExceptions(e);
                }
            }

        }

        /// <summary>
        /// Gets an events object which contains a new eventToken and 
        /// events that happened after the given eventToken was generated.
        /// </summary>
        /// <param name="eventToken">The event token that was retrieved by a call.</param>
        /// <returns>An events object which contains a new eventToken and events that happened after the given eventToken was generated</returns>
        /// <remarks>
        /// If for any reason event store is unable to retrieve events that happened after the
        /// event token was generated, an EventTokenExpiredException will be thrown. The caller
        /// is then expected to call GetCurrentEventToken() again before asking for new events.
        /// </remarks>
        /// <exception cref="CrowdEventTokenExpiredException">if events that happened after the event token was generated can not be retrieved.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdUnsupportedApiException">if the remote server does not support this operation.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments.</exception>
        public Events GetNewEvents(string eventToken)
        {
            try
            {
                // Our api resource
                string apiResource = "event/{0}.json".Fmt(eventToken);
                string httpMethod = "GET";

                //  Make the call:
                Events events = MakeAPICall<Events>(apiResource, httpMethod);

                return events;
            }
            catch (CrowdRestException e)
            {
                // The REST call failed, but we're talking to Crowd because otherwise ICSE would have been thrown
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    throw new CrowdUnsupportedApiException("1.2", "for event-based synchronisation");
                }
                else
                {
                    HandleEventTokenExpiredException(e.Error);
                    throw HandleCommonExceptions(e);
                }
            }
        }

        #endregion
    
    }
}

