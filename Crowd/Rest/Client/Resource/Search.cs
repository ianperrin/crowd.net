﻿using System;
using System.Collections.Generic;
using System.Net;
using ServiceStack.Text;
using Crowd.Rest.Client.Entity;

namespace Crowd.Rest.Client
{
    public partial class CrowdRestClientManager
    {

        #region API : Search

        public List<User> SearchUsers(int startIndex = 0, int maxResults = -1)
        {
            PropertySearchRestriction p = null;
            return SearchUsers(p, 0, -1);
        }

        /// <summary>
        /// Searches for users matching the specified criteria.
        /// </summary>
        /// <param name="searchRestriction">The search criteria.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of users satisfying the search restriction.</returns>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<User> SearchUsers(PropertySearchRestriction searchRestriction, int startIndex = 0, int maxResults = -1)
        {
            UserList users;
            try
            {
                // Our api resource
                // POST /search.json?entity-type=user
                // POST /search.json?entity-type=user&expand=user
                // GET /search?entity-type=user&restriction=email%3Dtest%40example.com
                string apiResource = "search.json?entity-type=user&expand=user&start-index={0}&max-results={1}".Fmt(
                                                startIndex.ToString()
                                                , maxResults.ToString()
                                                );

                //  Make the call:
                if (searchRestriction == null)
                {
                    string httpMethod = "GET";
                    users = MakeAPICall<UserList>(apiResource, httpMethod);
                }
                else
                {
                    string httpMethod = "POST";
                    users = MakeAPICall<UserList>(apiResource, httpMethod, searchRestriction);
                }
            }
            catch (CrowdRestException e)
            {
                throw HandleCommonExceptions(e);
            }

            return users.ToUserList();
        }

        /// <summary>
        /// Searches for users matching the specified criteria.
        /// </summary>
        /// <param name="searchRestriction">The search criteria using Crowd Query Language <see cref="https://developer.atlassian.com/display/CROWDDEV/Crowd+Query+Language"/>.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of users satisfying the search restriction.</returns>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<User> SearchUsers(string searchRestriction, int startIndex = 0, int maxResults = -1)
        {

            // TODO: Validate Crowd Query Language searching https://developer.atlassian.com/display/CROWDDEV/Crowd+Query+Language

            UserList users;
            try
            {
                // Our api resource
                // GET /search?entity-type=user&restriction=email%3Dtest%40example.com
                string apiResource = "search.json?entity-type=user&expand=user&start-index={0}&max-results={1}&restriction={2}".Fmt(
                                                startIndex.ToString()
                                                , maxResults.ToString()
                                                , searchRestriction
                                                );

                //  Make the call:
                string httpMethod = "GET";
                users = MakeAPICall<UserList>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                throw HandleCommonExceptions(e);
            }

            return users.ToUserList();
        }

        /// <summary>
        /// Searches for usernames matching the search criteria.
        /// </summary>
        /// <param name="searchRestriction">The search criteria.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of usernames satisfying the search restriction.</returns>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<string> SearchUserNames(PropertySearchRestriction searchRestriction = null, int startIndex = 0, int maxResults = -1)   
        {
            UserList users;
            try
            {
                // Our api resource
                // POST /search.json?entity-type=user
                // POST /search.json?entity-type=user&expand=user
                // GET /search?entity-type=user&restriction=email%3Dtest%40example.com
                string apiResource = "search.json?entity-type=user&start-index={0}&max-results={1}".Fmt(
                                                startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                //  Make the call:
                if (searchRestriction == null)
                {
                    string httpMethod = "GET";
                    users = MakeAPICall<UserList>(apiResource, httpMethod);
                }
                else
                {
                    string httpMethod = "POST";
                    users = MakeAPICall<UserList>(apiResource, httpMethod, searchRestriction);
                }
            }
            catch (CrowdRestException e)
            {
                throw HandleCommonExceptions(e);
            }

            return users.ToNameList();
        }

        /// <summary>
        /// Searches for usernames matching the specified criteria.
        /// </summary>
        /// <param name="searchRestriction">The search criteria using Crowd Query Language <see cref="https://developer.atlassian.com/display/CROWDDEV/Crowd+Query+Language"/>.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of users satisfying the search restriction.</returns>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<string> SearchUserNames(string searchRestriction, int startIndex = 0, int maxResults = -1)
        {

            // TODO: Validate Crowd Query Language searching https://developer.atlassian.com/display/CROWDDEV/Crowd+Query+Language

            UserList users;
            try
            {
                // Our api resource
                // GET /search?entity-type=user&restriction=email%3Dtest%40example.com
                string apiResource = "search.json?entity-type=user&expand=user&start-index={0}&max-results={1}&restriction={2}".Fmt(
                                                startIndex.ToString()
                                                , maxResults.ToString()
                                                , searchRestriction
                                                );

                //  Make the call:
                string httpMethod = "GET";
                users = MakeAPICall<UserList>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                throw HandleCommonExceptions(e);
            }

            return users.ToNameList();
        }

        /// <summary>
        /// Searches for groups matching the specified criteria.
        /// </summary>
        /// <param name="searchRestriction">The search criteria.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of groups satisfying the search restriction.</returns>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<Group> SearchGroups(PropertySearchRestriction searchRestriction = null, int startIndex = 0, int maxResults = -1)
        {
            GroupList groups;
            try
            {
                // Our api resource
                // POST /search.json?entity-type=group&expand=group
                string apiResource = "search.json?entity-type=group&expand=group&start-index={0}&max-results={1}".Fmt(
                                                startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                //  Make the call:
                if (searchRestriction == null)
                {
                    string httpMethod = "GET";
                    groups = MakeAPICall<GroupList>(apiResource, httpMethod);
                }
                else
                {
                    string httpMethod = "POST";
                    groups = MakeAPICall<GroupList>(apiResource, httpMethod, searchRestriction);
                }
            }
            catch (CrowdRestException e)
            {
                throw HandleCommonExceptions(e);
            }

            return groups.ToGroupList();
        }

        /// <summary>
        /// Searches for groups matching the specified criteria.
        /// </summary>
        /// <param name="searchRestriction">The search criteria.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of groups with attributes satisfying the search restriction.</returns>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<GroupWithAttributes> SearchGroupsWithAttributes(PropertySearchRestriction searchRestriction = null, int startIndex = 0, int maxResults = -1)
        {
            GroupWithAttributesList groups;
            try
            {
                // Our api resource
                // POST /search.json?entity-type=group&expand=group,attributes
                string apiResource = "search.json?entity-type=group&expand=group,attributes&start-index={0}&max-results={1}".Fmt(
                                                startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                //  Make the call:
                if (searchRestriction == null)
                {
                    string httpMethod = "GET";
                    groups = MakeAPICall<GroupWithAttributesList>(apiResource, httpMethod);
                }
                else
                {
                    string httpMethod = "POST";
                    groups = MakeAPICall<GroupWithAttributesList>(apiResource, httpMethod, searchRestriction);
                }
            }
            catch (CrowdRestException e)
            {
                throw HandleCommonExceptions(e);
            }

            return groups.ToGroupList();
        }

        /// <summary>
        /// Searches for group names matching the search criteria.
        /// </summary>
        /// <param name="searchRestriction">The search criteria.</param>
        /// <param name="startIndex">The starting index of the search results.</param>
        /// <param name="maxResults">The maximum number of results returned from the search.</param>
        /// <returns>List of group names satisfying the search restriction.</returns>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public List<string> SearchGroupNames(PropertySearchRestriction searchRestriction = null, int startIndex = 0, int maxResults = -1)
        {
            GroupList groups;
            try
            {
                // Our api resource
                // POST /search.json?entity-type=group
                string apiResource = "search.json?entity-type=group&start-index={0}&max-results={1}".Fmt(
                                                startIndex.ToString()
                                                , maxResults.ToString()
                                                );
                //  Make the call:
                if (searchRestriction == null)
                {
                    string httpMethod = "GET";
                    groups = MakeAPICall<GroupList>(apiResource, httpMethod);
                }
                else
                {
                    string httpMethod = "POST";
                    groups = MakeAPICall<GroupList>(apiResource, httpMethod, searchRestriction);
                }
            }
            catch (CrowdRestException e)
            {
                throw HandleCommonExceptions(e);
            }

            return groups.ToNameList();
        }

        #endregion

    }
}

