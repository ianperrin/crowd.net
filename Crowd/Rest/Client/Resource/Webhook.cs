﻿using System;
using System.Collections.Generic;
using System.Net;
using ServiceStack.Text;
using Crowd.Rest.Client.Entity;

namespace Crowd.Rest.Client
{
    public partial class CrowdRestClientManager
    {

        #region API : Webhook

        /// <summary>
        /// Gets a Webhook endpoint URL.
        /// </summary>
        /// <param name="webhookId">The webhook unique identifier, , as returned by RegisterWebhook(string, string)</param>
        /// <returns>The endpoint URL associated to the Webhook.</returns>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason.</exception>
        /// <exception cref="CrowdWebhookNotFoundException">if the Webhook is not registered</exception>
        public string GetWebhook(long webhookId)
        {
            try
            {
                // Our api resource
                string apiResource = "webhook/{0}.json".Fmt(webhookId);
                string httpMethod = "GET";

                //  Make the call:
                Webhook webhook = MakeAPICall<Webhook>(apiResource, httpMethod);

                return webhook.EndpointUrl;
            }
            catch (CrowdRestException e)
            {
                HandleWebhookNotFound(e.Error, webhookId);
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Registers a new Webhook on the server.
        /// </summary>
        /// <param name="endpointUrl">The URL of the HTTP endpoint provided by the application. Crowd will ping the Webhook by issuing a body-less POST request when new events are available. The Webhook must respond with an HTTP 2xx status code.</param>
        /// <param name="token">The token that Crowd will present when pinging the Webhook.</param>
        /// <returns>An identifier of the Webhook, that can be used to unregister it UnregisterWebhook(long)</returns>
        /// <remarks>
        /// The Webhook will be called back when the Crowd server produces new events. Applications are expected to request a new event GetCurrentEventToken() after registering a Webhook, so then can obtain the new events using CrowdClient.getNewEvents(string) in the Webhook callback.
        /// This operation is idempotent, i.e., an attempt to register a callback with the same endpointUrl as an existing Webhook already registered by the same application will return the identifier of the existing Webhook.
        /// If an application dies, or otherwise terminates without unregistering its Webhooks, Crowd reserves its rights to eventually unregister any Webhook that is no longer responding to the pings with an HTTP 2xx status code. Crowd may implement a fault-tolerant policy for Webhooks that are temporarily unreachable, but it is the application's responsibility to maintain the connection.
        /// </remarks>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason.</exception>
        public long RegisterWebhook(string endpointUrl, string token)
        {
            try
            {
                // Our api resource
                string apiResource = "webhook.json";
                string httpMethod = "POST";

                //  Create our arguments object:
                object args = new Webhook { EndpointUrl = endpointUrl, Token = token };

                //  Make the call:
                Webhook response = MakeAPICall<Webhook>(apiResource, httpMethod, args);

                return response.Id;
            }
            catch (CrowdRestException e)
            {
                // The REST call failed, but we're talking to Crowd because otherwise ICSE would have been thrown
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    throw new CrowdUnsupportedApiException("1.4", "for web hook synchronisation");
                }
                throw HandleCommonExceptions(e);
            }
        }

        /// <summary>
        /// Unregisters a Webhook on the server. 
        /// Well-behaved applications are expected to unregister their Webhooks when they no longer
        /// need them to free resources on the server. Otherwise, Crowd may unilaterally unregister
        /// the non-responding Webhooks, as described.
        /// </summary>
        /// <param name="webhookId">The identifier of the Webhook returned by RegisterWebhook(string, string).</param>
        /// <exception cref="CrowdApplicationPermissionException">if the Webhook identifier does not match any of the Webhooks previously registered by the application.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason.</exception>
        /// <exception cref="CrowdWebhookNotFoundException">if the Webhook is not found, or it not owned by the application, or the remote API does not support this operation</exception>
        public void UnregisterWebhook(long webhookId)
        {
            try
            {
                // Our api resource
                string apiResource = "webhook/{0}.json".Fmt(webhookId);
                string httpMethod = "DELETE";

                //  Make the call:
                MakeAPICall<Webhook>(apiResource, httpMethod);

            }
            catch (CrowdRestException e)
            {
                HandleWebhookNotFound(e.Error, webhookId);
                throw HandleCommonExceptions(e);
            }
        }

        #endregion


    }
}

