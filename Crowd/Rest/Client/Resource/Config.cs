﻿using System;
using System.Collections.Generic;
using System.Net;
using ServiceStack.Text;
using Crowd.Rest.Client.Entity;

namespace Crowd.Rest.Client
{
    public partial class CrowdRestClientManager
    {

        #region API : Configuration

        /// <summary>
        /// Gets the cookie configuration.
        /// </summary>
        /// <returns>
        /// The cookie configuration.
        /// </returns>
        /// <exception cref="CrowdApplicationPermissionException">if the application is not permitted to perform the requested operation on the server.</exception>
        /// <exception cref="CrowdInvalidAuthenticationException">if the application and password are not valid.</exception>
        /// <exception cref="CrowdOperationFailedException">if the operation has failed for any other reason, including invalid arguments and the operation not being supported on the server.</exception>
        public CookieConfiguration GetCookieConfiguration()
        {
            try
            {
                // Our api resource
                string apiResource = "config/cookie.json";
                string httpMethod = "GET";

                //  Make the call:
                return MakeAPICall<CookieConfiguration>(apiResource, httpMethod);
            }
            catch (CrowdRestException e)
            {
                throw HandleCommonExceptions(e);
            }
        }

        #endregion

    }
}

