﻿using System;

namespace Crowd.Rest.Client
{
    /// <summary>
    /// .NET API Wrapper for the Atlassian Crowd REST API.
    /// More information here: https://developer.atlassian.com/display/CROWDDEV/Crowd+REST+Resources
    /// https://docs.atlassian.com/atlassian-crowd/current/com/atlassian/crowd/integration/rest/service/RestCrowdClient.html
    /// </summary>
    public partial class CrowdRestClientManager : CrowdClient
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdRestClientManager" /> class.
        /// </summary>
        /// <param name="baseUrl">The base URL.</param>
        /// <param name="applicationName">Name of the application.</param>
        /// <param name="applicationPassword">The application password.</param>
        /// <param name="apiVersion">The API version.</param>
        public CrowdRestClientManager(string baseUrl, string applicationName, string applicationPassword, string apiVersion)
            : this(new ClientProperties(baseUrl, applicationName, applicationPassword), apiVersion)
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdRestClientManager"/> class.
        /// </summary>
        /// <param name="baseUrl">The base URL.</param>
        /// <param name="applicationName">Name of the application.</param>
        /// <param name="applicationPassword">The application password.</param>
        public CrowdRestClientManager(string baseUrl, string applicationName, string applicationPassword)
            : this(new ClientProperties(baseUrl, applicationName, applicationPassword))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdRestClientManager" /> class for
        /// interacting with a remote Crowd server.
        /// </summary>
        /// <param name="clientProperties">The crowd properties for the client.</param>
        /// <param name="apiVersion">The API version for the client</param>
        public CrowdRestClientManager(ClientProperties clientProperties, string apiVersion)
            : base(clientProperties, apiVersion)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrowdRestClientManager"/> class for 
        /// interacting with a remote Crowd server.
        /// </summary>
        /// <param name="clientProperties">The crowd properties for the client.</param>
        public CrowdRestClientManager(ClientProperties clientProperties)
            : base(clientProperties, "latest")
        {
        }

        #endregion
    }

}

